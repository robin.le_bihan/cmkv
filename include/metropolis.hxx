#include <cmath>

#include "fast-rand.hh"
#include "metropolis.hh"

template <typename T>
Metropolis<T>::Metropolis(neighborhood_f neighborhood, likelihood_f likelihood)
    : neighborhood_(neighborhood)
    , likelihood_(likelihood)
{}

template <typename T>
T Metropolis<T>::apply(const T& x_0, const double,
                       const unsigned max_iterations)
{
    auto x = x_0;
    auto l_x = likelihood_(x);
    auto rand = FastRand(42);
    unsigned it = 0;
    do
    {
        auto y = neighborhood_(x);
        const auto l_y = likelihood_(y);
        if (l_y > l_x) // or rand.next() <= alpha)
        {
            x = y;
            l_x = l_y;
        }
        if (it % 20 == 0)
        {
            std::cout << it << '/' << max_iterations << ' '
                      << l_x << std::endl;
        }
    } while (++it <= max_iterations);
    return x;
}
