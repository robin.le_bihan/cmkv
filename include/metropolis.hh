#pragma once

#include <functional>
#include "image.hh"

template <typename T>
class Metropolis {
public:
    using likelihood_f = std::function<double(const T&)>;
    using neighborhood_f = std::function<T(const T&)>;
    Metropolis(neighborhood_f neighborhood, likelihood_f likelihood);

    T apply(const T& x_0, const double temperature, const unsigned max_iterations = 10000);
    T operator()(const T& x_0, const double temperature, const unsigned max_iterations = 10000)
    {
        return apply(x_0, temperature, max_iterations);
    }

private:
    neighborhood_f neighborhood_;
    likelihood_f likelihood_;
};

#include "metropolis.hxx"