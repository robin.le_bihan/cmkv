#pragma once

#include <array>

#include "image.hh"

Image kmeans(const Image& image);

static constexpr RGBQUAD WHITE{255, 255, 255, 0};
static constexpr RGBQUAD BLACK{0, 0, 0, 0};
static constexpr RGBQUAD RED{0, 0, 255, 0};
static constexpr RGBQUAD GREEN{0, 255, 0, 0};
static constexpr RGBQUAD BLUE{255, 0, 0, 0};
static constexpr RGBQUAD MAGENTA{255, 0, 255, 0};
static constexpr RGBQUAD CYAN{255, 255, 0, 0};
static constexpr RGBQUAD YELLOW{0, 255, 255, 0};

static constexpr unsigned CLUSTERS_COUNT = 8;

template <typename T>
using clusters_array_t = std::array<T, CLUSTERS_COUNT>;

/**
 * Clusters initial colors.
 */
static constexpr clusters_array_t<RGBQUAD> base_colors{
    BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE};

/**
 * Compute the distance between two pixels.
 */
static constexpr unsigned distance(const RGBQUAD& c1, const RGBQUAD& c2)
{
    return std::abs(c1.rgbBlue - c2.rgbBlue) + std::abs(c1.rgbRed - c2.rgbRed)
        + std::abs(c1.rgbGreen - c2.rgbGreen);
}
