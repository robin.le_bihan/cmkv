#pragma once

#include <FreeImage.h>

class PixelIterator
{
    friend bool operator==(const PixelIterator& lhs, const PixelIterator& rhs);
    friend bool operator!=(const PixelIterator& lhs, const PixelIterator& rhs);

public:
    PixelIterator(FIBITMAP* bitmap, unsigned x, unsigned y);

    RGBQUAD operator*() const;
    PixelIterator& operator++();

private:
    FIBITMAP* bitmap_;
    unsigned x_;
    unsigned y_;
};
