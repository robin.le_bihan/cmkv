#pragma once

#include <FreeImage.h>
#include <string>
#include <vector>

#include "pixel-iterator.hh"

class Image
{
public:
    using const_iterator = PixelIterator;
    using point_t = std::pair<unsigned, unsigned>;

    Image(unsigned width, unsigned height);
    Image(const std::string& filename);
    ~Image();
    Image(const Image& other);
    Image(Image&& other);

    Image& operator=(const Image& other);
    Image& operator=(Image&& other) = delete;

    FREE_IMAGE_FORMAT get_fif();

    unsigned get_width() const
    {
        return FreeImage_GetWidth(bitmap_);
    }
    unsigned get_height() const
    {
        return FreeImage_GetHeight(bitmap_);
    }

    unsigned size() const
    {
        return get_height() * get_width();
    }

    RGBQUAD operator[](const std::pair<unsigned, unsigned>& coords) const;
    void set_pixel(unsigned x, unsigned y, RGBQUAD color);

    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    void save(const std::string& filename);
    unsigned nb_components() const;
    std::vector<unsigned> get_components() const;
    void update_component(const std::vector<unsigned>& mask, unsigned index,
                          const RGBQUAD& color);

private:
    void mark_component(std::vector<unsigned>& buffer, unsigned i, unsigned j,
                        const RGBQUAD& value, const unsigned index) const;
    std::string filename_;
    FIBITMAP* bitmap_;
};

inline bool operator==(const RGBQUAD& lhs, const RGBQUAD& rhs)
{
    return lhs.rgbBlue == rhs.rgbBlue && lhs.rgbGreen == rhs.rgbGreen
        && lhs.rgbRed == rhs.rgbRed;
}
