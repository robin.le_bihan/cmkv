#pragma once

#include <cstdlib>
#include <vector>

class FastRand {
public:
    FastRand();
    FastRand(unsigned seed);
    float next();
    int next(int min, int max);

private:
    std::vector<float> rand_array_;
    size_t index_ = 0;
};
