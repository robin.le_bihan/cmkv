#include "image.hh"

#include <stdexcept>
#include <utility>

Image::Image(unsigned width, unsigned height)
    : filename_()
    , bitmap_(FreeImage_Allocate(width, height, 24))
{}

Image::Image(const std::string& filename)
    : filename_(filename)
    , bitmap_(FreeImage_Load(get_fif(), filename_.c_str()))
{
    if (!bitmap_)
        throw std::runtime_error("failed to load image");
}

Image::Image(const Image& other)
    : filename_(other.filename_)
    , bitmap_(FreeImage_Copy(other.bitmap_, 0, 0, other.get_width(),
                             other.get_height()))
{}

Image::Image(Image&& other)
    : filename_(other.filename_)
    , bitmap_(std::exchange(other.bitmap_, nullptr))
{}

Image& Image::operator=(const Image& other)
{
    filename_ = other.filename_;
    bitmap_ = FreeImage_Copy(other.bitmap_, 0, 0, other.get_width(),
                             other.get_height());
    return *this;
}

Image::~Image()
{
    if (bitmap_)
        FreeImage_Unload(bitmap_);
}

static bool endsWith(const std::string& str, const std::string& suffix)
{
    return str.size() >= suffix.size()
        && 0 == str.compare(str.size() - suffix.size(), suffix.size(), suffix);
}

FREE_IMAGE_FORMAT Image::get_fif()
{
    if (endsWith(filename_, "png"))
        return FIF_PNG;
    if (endsWith(filename_, "jpg") or endsWith(filename_, "jpeg"))
        return FIF_JPEG;
    if (endsWith(filename_, "gif"))
        return FIF_GIF;
    throw std::runtime_error(
        "unknwon image format (supported: png, jpeg & gif)");
}

void Image::set_pixel(unsigned x, unsigned y, RGBQUAD color)
{
    if (!FreeImage_SetPixelColor(bitmap_, x, y, &color))
        throw std::out_of_range{"Out of range pixel coords"};
}

RGBQUAD Image::operator[](const point_t& coords) const
{
    RGBQUAD result;
    auto& [x, y] = coords;

    if (!FreeImage_GetPixelColor(bitmap_, x, y, &result))
        throw std::out_of_range{"Out of range pixel coords"};

    return result;
}

Image::const_iterator Image::begin() const noexcept
{
    return PixelIterator(bitmap_, 0, 0);
}

Image::const_iterator Image::end() const noexcept
{
    return PixelIterator(bitmap_, 0, get_height());
}

void Image::save(const std::string& filename)
{
    filename_ = filename;
    if (!FreeImage_Save(get_fif(), bitmap_, filename.c_str()))
        throw std::runtime_error("unable to save image");
}

void Image::mark_component(std::vector<unsigned>& buffer, unsigned i,
                           unsigned j, const RGBQUAD& value,
                           const unsigned index) const
{
    if (buffer[i * get_height() + j] != index && value == (*this)[{i, j}])
    {
        // Mark the pixel
        buffer[i * get_height() + j] = index;
        // Explore the component
        if (i > 0)
            mark_component(buffer, i - 1, j, value, index);
        if (j > 0)
            mark_component(buffer, i, j - 1, value, index);
        if (i < get_width() - 1)
            mark_component(buffer, i + 1, j, value, index);
        if (j < get_height() - 1)
            mark_component(buffer, i, j + 1, value, index);
    }
}

unsigned Image::nb_components() const
{
    auto buffer = std::vector<unsigned>(get_width() * get_height());
    unsigned count = 1u;

    for (auto i = 0u; i < get_width(); i++)
    {
        for (auto j = 0u; j < get_height(); j++)
        {
            if (!buffer[i * get_height() + j])
            {
                mark_component(buffer, i, j, (*this)[{i, j}], count);
                count++;
            }
        }
    }

    return count;
}

std::vector<unsigned> Image::get_components() const
{
    auto buffer = std::vector<unsigned>(get_width() * get_height());
    unsigned count = 1u;

    for (auto i = 0u; i < get_width(); i++)
    {
        for (auto j = 0u; j < get_height(); j++)
        {
            if (!buffer[i * get_height() + j])
            {
                mark_component(buffer, i, j, (*this)[{i, j}], count);
                count++;
            }
        }
    }

    return buffer;
}

void Image::update_component(const std::vector<unsigned>& mask, unsigned index,
                             const RGBQUAD& color)
{
    for (auto i = 0u; i < get_width(); i++)
        for (auto j = 0u; j < get_height(); j++)
            if (mask[i * get_height() + j] == index)
                set_pixel(i, j, color);
}
