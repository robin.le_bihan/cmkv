#include "pixel-iterator.hh"

#include <stdexcept>

PixelIterator::PixelIterator(FIBITMAP* bitmap, unsigned x, unsigned y)
    : bitmap_(bitmap)
    , x_(x)
    , y_(y)
{}

RGBQUAD PixelIterator::operator*() const
{
    RGBQUAD pixel;

    if (!FreeImage_GetPixelColor(bitmap_, x_, y_, &pixel))
        throw std::out_of_range{"Out of range pixel coords"};

    return pixel;
}

PixelIterator& PixelIterator::operator++()
{
    if (x_ == FreeImage_GetWidth(bitmap_) && y_ == FreeImage_GetHeight(bitmap_))
        return *this;

    x_++;
    if (x_ >= FreeImage_GetWidth(bitmap_) && y_ < FreeImage_GetHeight(bitmap_))
    {
        x_ = 0;
        y_++;
    }

    return *this;
}

bool operator==(const PixelIterator& lhs, const PixelIterator& rhs)
{
    return (lhs.bitmap_ == rhs.bitmap_
            && lhs.x_ == rhs.x_
            && lhs.y_ == rhs.y_);
}

bool operator!=(const PixelIterator& lhs, const PixelIterator& rhs)
{
    return !(lhs == rhs);
}
