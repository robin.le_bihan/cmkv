#include <FreeImage.h>
#include <iostream>
#include <limits>
#include <numeric>
#include <vector>

#include "fast-rand.hh"
#include "image.hh"
#include "kmeans.hh"
#include "metropolis.hh"

/**
   From FreeImage documentation (p. 4).
   FreeImage error handler
   @param fif Format / Plugin responsible for the error
   @param message Error message
*/
void FreeImageErrorHandler(FREE_IMAGE_FORMAT fif, const char* message)
{
    const auto format = (fif != FIF_UNKNOWN ? FreeImage_GetFormatFromFIF(fif) : "UNKNOWN");
    std::cout << "\n*** [" << format << "] " << message << "***" << std::endl;
}

int main(int argc, char* argv[])
{
    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " image_input.png"
                  << " image_output.png\n";
        return 1;
    }
    FreeImage_Initialise();
    FreeImage_SetOutputMessage(FreeImageErrorHandler);

    const auto in_path = std::string { argv[1] };
    const auto out_path = std::string { argv[2] };

    auto image = Image { in_path };
    auto first_result = kmeans(image);
    first_result.save("/tmp/kmeans.png");
    auto max_nb_components = first_result.nb_components();

    auto rand = FastRand(66);

    auto mutate = [&rand](const Image& last_image) {
        auto copy = last_image;
        auto mask = last_image.get_components();

        // Compute cost of components
        auto cost = std::vector<unsigned> {};
        for (const auto& e : mask) {
            if (cost.size() == e - 1)
                cost.push_back(1);
            else
                cost[e - 1]++;
        }

        // Compute cumulative cost
        auto cum = std::vector<double> {};
        for (const auto& e : cost) {
            if (cum.empty())
                cum.push_back(std::exp(last_image.size() / e));
            else
                cum.push_back(std::exp(last_image.size() / e) + cum.back());
        }

        // Normalize cum
        for (auto& e : cum)
            e /= cum.back();

        // Pick a component to mutate
        auto r = rand.next();
        unsigned pos = 0u;
        while (r > cum[pos])
            pos++;

        // Mutate the component
        auto new_color = base_colors[rand.next(0, CLUSTERS_COUNT - 1)];
        copy.update_component(mask, pos + 1, new_color);
        return copy;
    };

    auto likelihood = [&image, &max_nb_components](const Image& new_image) {
        size_t diff = std::inner_product(image.begin(), image.end(), new_image.begin(),
            0u, std::plus<unsigned> {}, distance);
        auto nb_components = new_image.nb_components();
        return ((1.0f - diff / (765.0f * new_image.size()))
                   + (max_nb_components - nb_components)
                       / (float)max_nb_components)
            / 2;
    };

    auto metro = Metropolis<Image>(mutate, likelihood);
    auto solution = metro(first_result, 1.0, 10000);

    solution.save(out_path);

    FreeImage_DeInitialise();
}
