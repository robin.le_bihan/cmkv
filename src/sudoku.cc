#include <FreeImage.h>
#include <iostream>
#include <limits>
#include <vector>

#include "fast-rand.hh"
#include "image-diff.hh"
#include "image.hh"
#include "kmeans.hh"
#include "metropolis.hh"

/**
   From FreeImage documentation (p. 4).
   FreeImage error handler
   @param fif Format / Plugin responsible for the error
   @param message Error message
*/
void FreeImageErrorHandler(FREE_IMAGE_FORMAT fif, const char* message)
{
    const auto format =
        (fif != FIF_UNKNOWN ? FreeImage_GetFormatFromFIF(fif) : "UNKNOWN");
    std::cout << "\n*** [" << format << "] " << message << "***" << std::endl;
}

using sudoku_t = std::vector<int>;

unsigned error_count(const sudoku_t& sudoku)
{
    unsigned count = 0;
    for (int y = 0; y < 9; ++y)
    {
        for (int x = 0; x < 9; ++x)
        {
            auto cell = sudoku[y * 9 + x];
            for (int xx = 0; xx < 9; ++xx)
            {
                if (xx != x and cell == sudoku[y * 9 + xx])
                {
                    count++;
                    break;
                }
            }
            for (int yy = 0; yy < 9; ++yy)
            {
                if (yy != y and cell == sudoku[yy * 9 + x])
                {
                    count++;
                    break;
                }
            }
            for (int yy = y - y % 3; yy < y + (3 - y % 3); ++yy)
            {
                for (int xx = x - x % 3; xx < x + (3 - x % 3); ++xx)
                {
                    if (yy != y and xx != x and cell == sudoku[yy * 9 + xx])
                    {
                        count++;
                        break;
                    }
                }
            }
        }
    }
    return count;
}

std::vector<unsigned> compute_cost(const sudoku_t& sudoku)
{
    auto cost = std::vector<unsigned>{};
    for (int y = 0; y < 9; ++y)
    {
        for (int x = 0; x < 9; ++x)
        {
            unsigned count = 0;
            auto cell = sudoku[y * 9 + x];
            for (int xx = 0; xx < 9; ++xx)
            {
                if (xx != x and cell == sudoku[y * 9 + xx])
                {
                    count++;
                    break;
                }
            }
            for (int yy = 0; yy < 9; ++yy)
            {
                if (yy != y and cell == sudoku[yy * 9 + x])
                {
                    count++;
                    break;
                }
            }
            for (int yy = y - y % 3; yy < y + (3 - y % 3); ++yy)
            {
                for (int xx = x - x % 3; xx < x + (3 - x % 3); ++xx)
                {
                    if (yy != y and xx != x and cell == sudoku[yy * 9 + xx])
                    {
                        count++;
                        break;
                    }
                }
            }
            cost.push_back(count);
        }
    }

    return cost;
}

int main(void)
{
    auto sudoku = std::vector<int>{
        9, 3, 7, 6, 0, 0, 1, 0, 5, 0, 0, 5, 8, 0, 0, 7, 0, 6, 0, 6, 0,
        5, 0, 0, 0, 4, 0, 0, 0, 0, 4, 2, 7, 0, 0, 3, 0, 0, 0, 0, 0, 0,
        5, 0, 0, 7, 2, 6, 9, 3, 0, 0, 0, 0, 3, 0, 4, 0, 6, 0, 0, 5, 0,
        0, 5, 0, 0, 0, 0, 3, 6, 0, 6, 8, 0, 0, 0, 0, 0, 0, 1};
    auto can_set = std::vector<bool>(81, false);
    for (int i = 0; i < 81; ++i)
        can_set[i] = sudoku[i] == 0;

    auto rand = FastRand(66);
    for (int i = 0; i < 81; ++i)
        if (sudoku[i] == 0)
            sudoku[i] = rand.next(1, 9);

    auto print = [](const sudoku_t& sudoku) {
        for (int y = 0; y < 9; ++y)
        {
            if (y % 3 == 0)
                std::cout << "-------------------------" << std::endl;
            for (int x = 0; x < 9; ++x)
            {
                if (x % 3 == 0)
                    std::cout << "| ";
                std::cout << sudoku[y * 9 + x] << ' ';
            }
            std::cout << "|" << std::endl;
        }
        std::cout << "-------------------------" << std::endl;
    };

    auto mutate = [&can_set, &rand](const sudoku_t& sudoku) {
        auto copy = sudoku;
        auto cost = compute_cost(copy);
        auto cum = std::vector<double>{};
        for (const auto& e : cost)
        {
            if (cum.empty())
                cum.push_back(std::exp(e));
            else
                cum.push_back(std::exp(e) + cum.back());
        }
        for (auto& e : cum)
            e /= cum.back();

        while (true)
        {
            auto r = rand.next();
            unsigned pos = 0u;
            while (r > cum[pos])
                pos++;
            if (not can_set[pos])
                continue;
            copy[pos] = rand.next(1, 9);
            break;
        }
        return copy;
    };

    auto likelihood = [](const sudoku_t& sudoku) {
        return 1.0 - (error_count(sudoku) / (3.0 * 81.0));
    };

    auto metro = Metropolis<sudoku_t>(mutate, likelihood);
    print(sudoku);
    std::cout << "#errors = " << error_count(sudoku) << std::endl;
    auto solution = metro(sudoku, 1.0, 150000);
    print(solution);
    std::cout << "#errors = " << error_count(solution) << std::endl;
}
