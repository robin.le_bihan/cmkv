#include "kmeans.hh"

#include <algorithm>
#include <functional>
#include <iostream>
#include <limits>
#include <numeric>

namespace
{
    struct kmean_metadata
    {
        size_t nr = 0;
        size_t ng = 0;
        size_t nb = 0;
        size_t count = 0;
    };

    /**
     * Return closest cluster index to color.
     */
    template <typename Iterator>
    unsigned closest_cluster(Iterator begin, Iterator end, const RGBQUAD& color)
    {
        clusters_array_t<unsigned> distances;
        std::transform(
            begin, end, std::begin(distances),
            [&color](const auto& cluster) { return distance(cluster, color); });
        auto min_it =
            std::min_element(std::cbegin(distances), std::cend(distances));
        return std::distance(std::cbegin(distances), min_it);
    }

    template <typename LIterator, typename RIterator>
    unsigned clusters_diff(LIterator lbegin, LIterator lend, RIterator rbegin)
    {
        return std::inner_product(lbegin, lend, rbegin, 0,
                                  std::plus<unsigned>{}, distance);
    }

} // namespace

/**
 * Reduce colors in image using kmeans
 */
Image kmeans(const Image& image)
{
    auto last_clusters = base_colors;
    auto clusters = base_colors;

    unsigned iterations_count = 0;
    do
    {
        clusters_array_t<kmean_metadata> metadata;
        last_clusters = clusters;

        std::for_each(std::begin(image), std::end(image),
                      [&metadata, &clusters](const auto pixel) {
                          auto best = closest_cluster(begin(clusters),
                                                      end(clusters), pixel);

                          // Update metadata
                          metadata[best].nr += pixel.rgbRed;
                          metadata[best].nb += pixel.rgbBlue;
                          metadata[best].ng += pixel.rgbGreen;
                          metadata[best].count++;
                      });

        // Update centroids
        auto clusters_it = begin(clusters);
        for (const auto& meta : metadata)
        {
            if (meta.count != 0)
            {
                *clusters_it =
                    RGBQUAD{static_cast<BYTE>(meta.nb / meta.count),
                            static_cast<BYTE>(meta.ng / meta.count),
                            static_cast<BYTE>(meta.nr / meta.count), 0};
            }

            clusters_it++;
        }

        iterations_count++;
    } while (0 < clusters_diff(begin(clusters), end(clusters),
                               begin(last_clusters)));

    std::cout << "k-means completed in " << iterations_count << " iterations\n";

    // Associate clusters
    auto res = Image{image.get_width(), image.get_height()};

    for (auto i = 0u; i < image.get_width(); i++)
    {
        for (auto j = 0u; j < image.get_height(); j++)
        {
            auto color = image[{i, j}];
            auto best = closest_cluster(begin(clusters), end(clusters), color);

            res.set_pixel(i, j, base_colors[best]);
        }
    }

    return res;
}
