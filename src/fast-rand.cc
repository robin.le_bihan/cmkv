#include "fast-rand.hh"

#include <ctime>

FastRand::FastRand()
    : FastRand(std::time(0))
{
}

FastRand::FastRand(unsigned seed)
{
    std::srand(seed);
    const static size_t LARGE_PRIME_NUMBER = 6700417;
    rand_array_.reserve(LARGE_PRIME_NUMBER);
    for (size_t i = 0; i < LARGE_PRIME_NUMBER; i++)
        rand_array_.push_back((float)std::rand() / RAND_MAX);
}

float FastRand::next()
{
    index_ = (index_ + 1) % rand_array_.size();
    return rand_array_[index_];
}

int FastRand::next(int min, int max)
{
    auto x = next() * (max - min + 1) + min;
    return x > max ? max : x;
}