CMKV project &mdash; Image simplification
=========================================

Installation & usage
--------------------

    $ cd cmkv
    $ make
    $ ./cmkv input_image.[png|jpg|gif] output_image.[png|jpg|gif]

WARNING! May take a while to return for large images! For the reference, on an Intel 8th-gen i7 2.2GHz core, it takes ~5 minutes to compute the parrot image (384x256) below! 

Pipeline
--------

1. K-Means to have a irregular estimation of the expected result.
2. Regularization using the Metropolis algorithm

### K-Means

K-means algorithm is implemented in the `kmeans` function declared in `kmeans.hh` and defined in `kmeans.cc`. It takes an input image and will return the image with a reduced color palette.

The implementation is pretty simple, we begin by creating 8 clusters corresponding to our 8 base colors (black, white, red, green, blue, cyan, magenta and yellow). For each pixel in our image, we compute the closest cluster and assign the pixel to the latter. Once done, we recompute the cluster centroid as the average of all the pixels assigned to the cluster. The process is repeated until the clusters are not updated anymore.

In order to compute the image, we go through each pixel, and assign it the original color of its associated cluster.

### Metropolis

The algorithm is implemented in the `Metropolis<T>` class. It is templated on the type of the data it works on (in our case, `Image`). It also requires two `std::function`:
- a neighborhood function (`std::function<T(const T&)>`) which returns a copy of its argument with **one** randomly mutated dimension (choosen randomly or not) ;
- a likelihood function (`std::function<double(const T&)>`).

Then, `Metropolis::apply(const T& x_0, const double temperature, const unsigned max_iterations = 10000)` runs the algorithm with `x_0` as starting point.

As explained earlier, for the image simplification problem, the Metropolis algorithm takes the result of the K-Means as `x_0`. Therefore, *one dimension of the problem corresponds to one **cluster** instead of one pixel*. The range of value each dimension can take is the set of 8 colors.

Given that, our neighborhood function chooses one of the smallest clusters and changes the color of each of its pixels. (It chooses the cluster randomly once they've been ordered by size through a cumulative sum.) Our likelihood function attributes a probabiliy score considering the number of connected components and the similarity with the original image.

Currently, the number of iterations is fixed to 10^5.


A couple examples
-----------------

The leftmost image is the input image. The middle image is the result as returned by the K-Means and the rightmost image is the final output.

![cmkv on a parrot](examples/parrot-demo.png)
![cmkv on Lena](examples/lena-demo.png)

Bonus
-----

To test our Metropolis implementation on a simpler problem, we implemented a sudoku solver. It sometimes yields wrong or sub-optimal results but it definitely converges.

To run it:

    make sudoku && ./sudoku
