CXX= g++
CXXFLAGS= -Wall -Wextra -pedantic -std=c++17 -Iinclude/
LDLIBS= -lfreeimage

VPATH= src/

BIN= cmkv
OBJS= image.o fast-rand.o pixel-iterator.o kmeans.o
OBJ_CMKV= cmkv.o
OBJ_SUDOKU= sudoku.o


all: release


debug: CXXFLAGS += -g -O0
debug: $(BIN)


release: CXXFLAGS += -Ofast -Werror
release: $(BIN)

sudoku: $(OBJS) $(OBJ_SUDOKU)
	$(CXX) $(CXXFLAGS) $(LDLIBS) $(OBJS) $(OBJ_SUDOKU) -o $@


$(BIN): $(OBJS) $(OBJ_CMKV)
	$(CXX) $(CXXFLAGS) $(LDLIBS) $(OBJS) $(OBJ_CMKV) -o $@


clean:
	$(RM) $(OBJS) $(OBJ_SUDOKU) $(OBJ_CMKV) $(BIN)
